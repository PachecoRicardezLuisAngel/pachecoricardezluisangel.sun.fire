<?php

function invalido($indicador) {
  if (strlen($indicador) < 10) {
    return true;
  }
  /* ¿Algo más por validar? */
}

 $app->container->singleton('db', function () {
  return new PDO("pgsql:host=148.208.228.139 port=1111 user=a09161253 password=Peluches4 dbname=a09161253");
});

$app->get('/xml', function() use($app) {
  /* Será usado por app/templates/xml.php */
  $datos = array(
    'ce2009' => array(
    )
  );

  /* Selección en la bd */
  $ce2009 = $app->db->query("select e.desc_entidad as entidad,m.desc_municipio as municipio,
t.desc_tema as tema_nivel_1,t1.desc_tema as tema_nivel_2,t2.desc_tema tema_nivel_3,ce.medida as medida,
ce.unidad as unidad, n.desc_nota as nota, ce.cve_indicador as indicador
from ce2009 ce
inner join entidad e
on ce.cve_entidad=e.cve_entidad
inner join municipio m
on ce.cve_entidad=m.cve_municipio
inner join tema t
on ce.tema_nivel_1=t.cve_tema
inner join tema t1
on ce.tema_nivel_2=t1.cve_tema
inner join tema t2
on ce.tema_nivel_3=t2.cve_tema
inner join nota n
on ce.nota=n.cve_nota");
  if (!$ce2009) {
    /* Hubo algún error al seleccionar los datos de la bd */
    $app->halt(500, "Error: En estos momentos no se puede acceder a la información de los indicadores.");
  }

  /* En caso de exito, se genera el documento XML */
  foreach ($ce2009 as $indica) {
    $datos['ce2009'][] = $indica;
  }
  $app->response->headers->set('Content-Type', 'application/xml');
  $app->response->setStatus(200);
  $app->render('xml.php', $datos);
});






$app->get('/xml/:id', function($id) use($app) {
  /* Será usado por app/templates/xml.php */
  $data = array(
    'consulta' => array(
    )
  );

if (invalido($id)) {
    $app->halt(400, "Error: El id '$id' no cumple con el formato correcto.");
  }

  /* Selección en la bd */
$indicador=$id;
  $consulta="select e.desc_entidad as entidad,m.desc_municipio as municipio,
t.desc_tema as tema_nivel_1,t1.desc_tema as tema_nivel_2,t2.desc_tema tema_nivel_3,ce.medida as medida,
ce.unidad as unidad, n.desc_nota as nota, ce.cve_indicador as indicador
from ce2009 ce
inner join entidad e
on ce.cve_entidad=e.cve_entidad
inner join municipio m
on ce.cve_entidad=m.cve_municipio
inner join tema t
on ce.tema_nivel_1=t.cve_tema
inner join tema t1
on ce.tema_nivel_2=t1.cve_tema
inner join tema t2
on ce.tema_nivel_3=t2.cve_tema
inner join nota n
on ce.nota=n.cve_nota
where ce.cve_indicador like '".$indicador."%' ";
  $qry = $app->db->prepare("".$consulta);

//$qry->bindParam(':indicador', $id);

  if ($qry->execute() === false) {
    /* Algún error se presentó con la bd */
    $app->halt(500, "Error: No se ha podido encontrar el indicador con clave '$id'.");
  }

  if (!$qry) {
    /* Hubo algún error al seleccionar los datos de la bd */
    $app->halt(500, "Error: En estos momentos no se puede acceder a la información.");
  }
  if ($qry->rowCount() == 0) {
    /* No se borró ningún libro en la bd */
    $app->halt(404, "Error: El indicador con clave '$id' no existe.");
  }

  /* En caso de exito, se genera el documento XML */
  foreach ($qry as $return) {
    $dato['consulta'][] = $return;
  }
  $app->response->headers->set('Content-Type', 'application/xml');
  $app->response->setStatus(200);
  $app->render('pid.php', $dato);
});

































