<?php
require '../../app/vendor/autoload.php';
$app = new \Slim\Slim();
$app->config(array(
    'templates.path' => '../../app/templates/'
));

require '../../app/routes/root.php';
require '../../app/routes/xml.php';

$app->run();
